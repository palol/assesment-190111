# Useful resources

## Levenshtein distance
- http://levenshtein.net/
- http://levenshtein.net/levenshtein_links.htm
- https://people.cs.pitt.edu/~kirk/cs1501/Pruhs/Spring2006/assignments/editdistance/Levenshtein%20Distance.htm
- https://planetcalc.com/1721/

## AJAX and Twig
- https://www.tutorialspoint.com/symfony/symfony_ajax_control.htm
- https://stackoverflow.com/questions/14154670/how-ajax-calls-work-with-twig

## General
- bash check if php module is installed: `$ php -m | grep modulename`
- bash check if composer is installed: `$ composer show`
- bash check if laravel is installed: `$ php artisan --version`
