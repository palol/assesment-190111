<?php
require_once '../vendor/autoload';
require_once 'FilesystemResources.php';

use \Twig\Loader\FilesystemLoader;
use \Twig\Environment;

Class Renderer extends FilesystemResources {

  private $fl;
  private $te;

    public __construct() {
        parent::__construct();
        $this->fl = new FilesystemLoader( $this->get);
        $this->te = new Environment( $this->fl, ['cache' => $this->compilation_cache]);
    }

}
